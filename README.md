## සිංහල වචන
https://sinha.la/words/suggest/ වෙත යොමු කරන ලද වචන, විවිධ මූලාශ්‍ර භාවිතයෙන් නිවැරදි බව තහවුරු කර මෙහි ඇතුළත් කර ඇත.

### බලපත්‍රය
`Creative Commons Zero v1.0 Universal`
